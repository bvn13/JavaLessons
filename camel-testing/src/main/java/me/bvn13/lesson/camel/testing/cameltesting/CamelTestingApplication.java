package me.bvn13.lesson.camel.testing.cameltesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CamelTestingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CamelTestingApplication.class, args);
    }

}
